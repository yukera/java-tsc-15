package com.tsc.jarinchekhina.tm.exception.system;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not supported...");
    }

}
